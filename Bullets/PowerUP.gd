extends Area2D

var type = null

func _ready():
    pass


func _process(delta):
    position.y += 250 * delta


func _on_PowerUP_area_entered(area):
    if area.has_method("powerUp"):
        remove_powerUp()
        area.powerUp(type)


func _on_screen_exited():
    remove_powerUp()


func remove_powerUp():
    queue_free()