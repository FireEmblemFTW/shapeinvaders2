extends KinematicBody2D

export (float) var SPEED 

signal enemyKilled

var screensize
var speed = 200
var velocity = Vector2()

func _ready():
    screensize = get_viewport_rect().size
    

func _process(delta):
    position += velocity * delta * speed


func _on_screen_exited():
    position.y = -32
    

func die():
    #Called when the enemy is killed
    #Plays the appropriate sound/animation
    #Starts a timer to leave it on the screen momentarily
    #Emits a signal to main
    $CollisionShape2D.disabled = true
    $AnimatedSprite.animation = "dead"
    $Explosion.play()
    $Pause.start()
    emit_signal("enemyKilled", position)
    


func _on_Pause_timeout():
    #Removes the enemy when the pause timer ends
    queue_free()
