extends Area2D

var speed = 500
var velocity = Vector2(0,-1)

signal bulletHit

func _ready():
    # Called every time the node is added to the scene.
    # Initialization here
    pass


func _process(delta):
    position += velocity * delta * speed


func _on_screen_exited():
    remove_bullet()


func _on_Bullet_body_entered(body):
    #Check if the entered body can die
    #If it can, remove the bullet  and call the body's die function
    if body.has_method("die"):
        remove_bullet()
        body.die()    


func remove_bullet():
    queue_free()