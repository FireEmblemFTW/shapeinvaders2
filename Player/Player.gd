extends Area2D

signal playerHit
signal fire
signal pUpCollected
# A note on talking between scenes:
    # 1. create a signal
    # 2. connect an event and have it emit said signal
    # 3. in the main scene connect the desired scene with: instanceName.connect("signalName", self, "function_name")

export (int) var SPEED  # how fast the player will move (pixels/sec)

var screensize  # size of the game window
var alive = true
var superLazer = false

func _ready():

    screensize = get_viewport_rect().size

func _process(delta):
    
    #Players movement
    var velocity = Vector2() # the player's movement vector
    
    if Input.is_action_just_pressed("fire") && alive == true:
        fire()
    if Input.is_action_pressed("fire") && alive == true && superLazer == true:
        fire()
    if Input.is_action_pressed("ui_right") && alive == true:
        velocity.x += 1
    if Input.is_action_pressed("ui_left") && alive == true:
        velocity.x -= 1
    if Input.is_action_pressed("ui_down") && alive == true:
        velocity.y += 1
    if Input.is_action_pressed("ui_up") && alive == true:
        velocity.y -= 1
    if velocity.length() > 0:
        velocity = velocity.normalized() * SPEED
        $AnimatedSprite.play()
    else:
        $AnimatedSprite.stop()
    
    position += velocity * delta
    position.x = clamp(position.x, 15, screensize.x - 15)
    position.y = clamp(position.y, 15, screensize.y - 15)

    if velocity.x != 0 && alive == true:
        $AnimatedSprite.animation = "right"
        $AnimatedSprite.flip_v = false
        $AnimatedSprite.flip_h = velocity.x < 0
    elif alive == true:
        $AnimatedSprite.animation = "default" 

func fire():
    $Pew.play()
    emit_signal("fire")

func _on_Player_body_entered(body):
    alive = false
    $CollisionShape2D.disabled = true
    $AnimatedSprite.animation = "explode"
    $Explosion.play()
    $DeathPause.start()
    if body.has_method("die"):
        body.die()
    emit_signal("playerHit")
    yield($DeathPause, "timeout") 
    alive = true
    $CollisionShape2D.disabled = false
    position.x = 240
    position.y = 544
    
#Called when the plaer grabs a power-up, the type is passed from the corresponding power-up
func powerUp(type):
    emit_signal("pUpCollected", type)
    if type == "superLazer":
        superLazer = true
    else:
        superLazer = false
