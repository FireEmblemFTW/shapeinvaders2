extends Node


var BackgroundSpeed = 22
var screensize
var score = 0
var level = 0
var lives = 3
var indegoSpawn = Vector2(16,8)
var multiCounter = 0
var lazerCounter = 0
var gameInProgress = false

export (PackedScene) var Bullet #allows you to instance an outside scene
export (PackedScene) var SageSnowflake
export (PackedScene) var CyanCircle
export (PackedScene) var IndegoInfinity
export (PackedScene) var TopazTriangles
export (PackedScene) var OrangeOctagon
export (PackedScene) var EbonyX
export (PackedScene) var RedRhombus
export (PackedScene) var MultiShot
export (PackedScene) var SuperLazerUp
export (PackedScene) var SuperLazer

func _ready():
    randomize()
    screensize = get_viewport().size
    
func new_Game():
    score = 0
    level = 0
    lives = 3
    multiCounter = 0
    lazerCounter = 0
    indegoSpawn = Vector2(16,8)
    $HUD.show_Message(str("Lives: ", lives))
    $HUD.update_Score(score)
    $GameTimer.start()
    $EnemyTimer.start()

func _process(delta):
    # Called every frame. Delta is time since last frame.
    # Update game logic here.
    
    #Implimentation of scrolling background
    
    if $Background1.position.y <= screensize.y:
        $Background1.position.y += BackgroundSpeed
    else:
        $Background1.position.y = -screensize.y
    
    if $Background2.position.y <= screensize.y:
        $Background2.position.y += BackgroundSpeed
    else:
        $Background2.position.y = -screensize.y
    
    if Input.is_action_just_pressed("fire") && gameInProgress ==  false:
        new_Game()
        gameInProgress = true

# Handles the logic for creating and moving instances of bullets
# It is at this point I wish gdscript had switch statements.
func _on_Player_fire():
    if multiCounter == 0 && lazerCounter == 0:
        regular_shot()
    elif lazerCounter == 1:
        SuperLazer()
    elif lazerCounter >= 2:
        SuperLazer()
        two_SuperLazer()
    elif multiCounter == 1:
        regular_shot()
        one_Multishot()
    elif multiCounter == 2:
        regular_shot()
        one_Multishot()
        two_Multishot()
    elif multiCounter == 3:
        regular_shot()
        one_Multishot()
        two_Multishot()
        three_Multishot()
    elif multiCounter >= 4:
        regular_shot()
        one_Multishot()
        two_Multishot()
        three_Multishot()
        four_Multishot()

func regular_shot():
    var bullet = Bullet.instance()
    bullet.position = $Player.position 
    bullet.position.y -= 12    #Adjusted spawn position
    add_child(bullet)
    
func SuperLazer():
    var lazer = SuperLazer.instance()
    lazer.position = $Player.position 
    lazer.position.y -= 12    #Adjusted spawn position
    add_child(lazer)
    
func two_SuperLazer():
    var lazer2 = SuperLazer.instance()
    lazer2.position = $Player.position 
    lazer2.position.y -= 12    #Adjusted spawn position
    lazer2.velocity = Vector2(0,1)
    add_child(lazer2)

func one_Multishot():
    var bullet2 = Bullet.instance()
    bullet2.position = $Player.position 
    bullet2.position.y -= 12    #Adjusted spawn position
    bullet2.velocity = Vector2(1,-1)
    bullet2.rotation_degrees = 45
    add_child(bullet2)
    var bullet3 = Bullet.instance()
    bullet3.position = $Player.position 
    bullet3.position.y -= 12    #Adjusted spawn position
    bullet3.velocity = Vector2(-1,-1)
    bullet3.rotation_degrees = -45
    add_child(bullet3)

func two_Multishot():
    var bullet4 = Bullet.instance()
    bullet4.position = $Player.position 
    bullet4.position.y -= 12    #Adjusted spawn position
    bullet4.velocity = Vector2(1,0)
    bullet4.rotation_degrees = 90
    add_child(bullet4)
    var bullet5 = Bullet.instance()
    bullet5.position = $Player.position 
    bullet5.position.y -= 12    #Adjusted spawn position
    bullet5.velocity = Vector2(-1,0)
    bullet5.rotation_degrees = -90
    add_child(bullet5)

func three_Multishot():
    var bullet6 = Bullet.instance()
    bullet6.position = $Player.position 
    bullet6.position.y -= 12    #Adjusted spawn position
    bullet6.velocity = Vector2(1,1)
    bullet6.rotation_degrees = 135
    add_child(bullet6)
    var bullet7 = Bullet.instance()
    bullet7.position = $Player.position 
    bullet7.position.y -= 12    #Adjusted spawn position
    bullet7.velocity = Vector2(-1,1)
    bullet7.rotation_degrees = -135
    add_child(bullet7)
    
func four_Multishot():
    var bullet8 = Bullet.instance()
    bullet8.position = $Player.position 
    bullet8.position.y -= 12    #Adjusted spawn position
    bullet8.velocity = Vector2(0,1)
    bullet8.rotation_degrees = 180
    add_child(bullet8)

func _on_EnemyTimer_timeout():
    
    #SageSnowflake
    # choose a random location on Path2D
    $Path2D/PathFollow2D.set_offset(randi())
    # create an instance and add it to the scene
    var sageSnowflake = SageSnowflake.instance()
    # put it in a container to keep things organized
    $EnemyContainer.add_child(sageSnowflake)
    # set the mob's position to a random location
    sageSnowflake.position = $Path2D/PathFollow2D.position
    # connect a signal for when the enemy dies.
    sageSnowflake.connect("enemyKilled", self, "_on_Enemy_enemyKilled")
    
    #IndegoInfinity
    spawn_Indego()
    $ExtraIndego.start()
    
    if level >= 1:
        #OrangeOctagons
        $Path2D/PathFollow2D.set_offset(randi())
        var orangeOctagon = OrangeOctagon.instance()
        $EnemyContainer.add_child(orangeOctagon)
        orangeOctagon.position = $Path2D/PathFollow2D.position
        orangeOctagon.connect("enemyKilled", self, "_on_Enemy_enemyKilled")
    
    if level >= 2:
        #CyanCircle
        $Path2D/PathFollow2D.set_offset(randi())
        var cyanCircle = CyanCircle.instance()
        $EnemyContainer.add_child(cyanCircle)
        cyanCircle.position = $Path2D/PathFollow2D.position
        cyanCircle.connect("enemyKilled", self, "_on_Enemy_enemyKilled")
    
    if level >= 3:
        #EbonyX
        var ebonyX = EbonyX.instance()
        $EnemyContainer.add_child(ebonyX)
        ebonyX.position = Vector2(rand_range(0, screensize.x), rand_range(0, screensize.y))
        ebonyX.connect("enemyKilled", self, "_on_Enemy_enemyKilled")
    
    if level >= 5:    
        #TopazTriangles
        $Path2D/PathFollow2D.set_offset(randi())
        var topazTriangles = TopazTriangles.instance()
        $EnemyContainer.add_child(topazTriangles)
        topazTriangles.position = $Path2D/PathFollow2D.position
        topazTriangles.connect("enemyKilled", self, "_on_Enemy_enemyKilled")
    
    if level >= 7:
        #RedRhombus
        $Path2D/PathFollow2D.set_offset(randi())
        var redRhombus = RedRhombus.instance()
        $EnemyContainer.add_child(redRhombus)
        redRhombus.position = $Path2D/PathFollow2D.position
        redRhombus.connect("enemyKilled", self, "_on_Enemy_enemyKilled")

func _on_Enemy_enemyKilled(enemyPosition):
    score += 10
    $HUD.update_Score(score)
    if level >= 3:
        if randf() > 0.95:
            if randf() >= 0.5:
                var multiShot = MultiShot.instance()
                multiShot.position = enemyPosition
                add_child(multiShot)
            else:
                var superLazerUp = SuperLazerUp.instance()
                superLazerUp.position = enemyPosition
                add_child(superLazerUp)

func spawn_Indego():
    var indegoInfinity = IndegoInfinity.instance()
    $EnemyContainer.add_child(indegoInfinity)
    indegoInfinity.position = indegoSpawn
    indegoSpawn.x += 32
    if indegoSpawn.x >= screensize.x:
        indegoSpawn.x = 16
        indegoSpawn.y += 16
    if indegoSpawn.y >= screensize.y - 64:
        indegoSpawn.y = 8
    indegoInfinity.connect("enemyKilled", self, "_on_Enemy_enemyKilled")
    
func _on_ExtraIndego_timeout():
    #MoreIndegoInfinity
    spawn_Indego()

func _on_GameTimer_timeout():
    #Increase difficulty as time goes on
    level += 1
    if level == 4:
        $EnemyTimer.wait_time -= 0.025
    if level == 6:
        $EnemyTimer.wait_time -= 0.025
    if level == 8:
        $EnemyTimer.wait_time -= 0.05
    if level >= 10 && $EnemyTimer.wait_time > 0.05:
        $EnemyTimer.wait_time -= 0.05


func _on_Player_pUpCollected(type):
    if type == "multiShot":
        lazerCounter = 0
        multiCounter += 1
    if type == "superLazer":
        multiCounter = -1 #-1 so that the first multi collected after superlazer sets back to regular shot
        lazerCounter += 1


func _on_playerHit():
    lives -= 1
    if lives <= 0:
        game_Over()
    else:
        $GameTimer.stop()
        $EnemyTimer.stop()
        $HUD.show_Message(str("Lives: ", lives))
        $shortPause.start()
        yield($shortPause, "timeout")
        for child in $EnemyContainer.get_children():
	        child.queue_free()
        $GameTimer.start()
        $EnemyTimer.start()

func game_Over():
    $GameTimer.stop()
    $EnemyTimer.stop()
    $ExtraIndego.stop()
    for child in $EnemyContainer.get_children():
	        child.queue_free()
    $HUD.game_Over("GAME OVER\nScore: " + str(score))
    $GameOverTimer.start()
    yield($GameOverTimer, "timeout")
    gameInProgress = false
    