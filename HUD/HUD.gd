extends CanvasLayer

signal start_game

func show_Message(text):
    $MessageLabel.text = text
    $MessageLabel.show()
    $MessageTimer.start()

func game_Over(text):
    show_Message(text)
    $MessageTimer.stop()
    $GameOverTimer.start()
    yield($GameOverTimer, "timeout") #pauses the function execution until the timer times out
    $MessageLabel.text = "Press Space To Start"
    $MessageLabel.show()

func update_Score(score):
    $ScoreLabel.text = str(score)

func _on_MessageTimer_timeout():
    $MessageLabel.hide()
